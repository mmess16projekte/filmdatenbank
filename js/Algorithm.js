/**
 * This module contains the logic of the recommender algorithm. The current users ratings
 * will be used to match users who have rated matching movies with similar ratings.
 * The ratings of the identified similar user will be used to recommend movies to the the current user.
 */

/* global $ _ MovieDatabaseApp */

MovieDatabaseApp.Algorithm = function (firebase) {

    var that = {},
        firebaseRef = firebase,
        currentUserId = localStorage.getItem("userId"),
        currentMovieItems = [],
        onErrorCallback,
        onGetRecommendationsSuccessCallback;


    //stores all Ratings (also the User Ratings in a Dictionary in Vector format)
    function getRecommendations(similarityFactor) {
        var ratingVectorsDict = {};

        firebaseRef.child("movieRatings").once("value", function (dataSnapshot) {

            if (dataSnapshot.val() != null) {
                if (Object.keys(dataSnapshot.val()).length > 2) {
                    dataSnapshot.forEach(function (childSnapshot) {
                        generateVectors(ratingVectorsDict, childSnapshot);
                    });
                    generateRecommendations(ratingVectorsDict, similarityFactor);
                } else {
                    //console.log("Not Enough Ratings ERROR");
                    onErrorCallback();
                    return;
                }
            } else {
                //console.log("No Ratings in Database ERROR");
                onErrorCallback();
                return;
            }
        }
        );
    }

    //reads all ratings (upvote/downvote) from all users and transforms them, so that every user has a vector with
    //movieIds (as keys) and the rating (+1 or -1) (as values)
    function generateVectors(ratingVectorsDict, childSnapshot) {
        var vectorDict = {};
        var key = childSnapshot.key().split("___");     //key[0] equals type of rating (upvote or downvote)
                                                        //key[1] equals userId
        var i;
        var currentVal;
        var oldVal;
        var data = _.values(childSnapshot.val());

        for (i = 0; i < data.length; i++) {         //data[i] is the movieId

            switch (key[0]) {
            case "downvote":
                vectorDict[data[i]] = -1;
                break;
            case "upvote":
                vectorDict[data[i]] = +1;
                break;
            default:
                //console.log("Movie Ratings have been saved under wrong identifier");

            }

            currentVal = {};
            oldVal = ratingVectorsDict[key[1]];

            if (!oldVal) {
                currentVal = vectorDict;
            } else {
                currentVal = oldVal;
                _.extend(currentVal, vectorDict);
            }

            ratingVectorsDict[key[1]] = currentVal;
        }
    }

    //the previously generated vectors are processed to generate the final recommendations
    function generateRecommendations(ratingVectorsDict, similarityFactor) {
        var resultRecommendations;              //holds final movie recomendations (movieIds)
        var resultUserRanking = [];                 //holds final user ranking (which user has rated the same movies
                                                    //similar to the current user)
        var filteredRatingVectorsDict = {};         //holds only users (userIds) that have rated a minimum amount of
                                                    //movies that the current user has rated
        var usersMoviesMatchRanking;
        var userVector = detachUserVector(ratingVectorsDict);

        if (userVector) {
            usersMoviesMatchRanking = createMoviesMatchRanking(userVector, ratingVectorsDict);

            filterMoviesMatchRanking(usersMoviesMatchRanking, filteredRatingVectorsDict, ratingVectorsDict, similarityFactor);
            generateUserSimilarityRanking(userVector, filteredRatingVectorsDict, resultUserRanking);
            resultRecommendations = addRecommendedMovies(userVector, ratingVectorsDict, resultUserRanking);
            detachMoviesInListsFromRecommendations(resultRecommendations, getMovieItemsFromFirebase);

            return resultRecommendations;
        } else {
            //console.log("User has not rated any movies so far ERROR");
            onErrorCallback();
        }
        return resultRecommendations;
    }

    //removes the vector of currentUser from ratingVectorsDict and returns it
    function detachUserVector(ratingVectorsDict) {
        var userVector = ratingVectorsDict[currentUserId];
        delete ratingVectorsDict[currentUserId];
        return userVector;
    }

    //returns a ranking of users (userIds) based on how many movies they have rated, that the current user has also rated
    function createMoviesMatchRanking(userVector, ratingVectorsDict) {
        var counter;
        var userSimilarityDict = {};
        var user;
        var compareVector;
        var movieId;

        for (user in ratingVectorsDict) {
            compareVector = ratingVectorsDict[user];
            counter = 0;
            for (movieId in compareVector) {
                if (userVector[movieId]) {
                    counter++;
                }
            }
            userSimilarityDict[user] = counter;
        }
        return sortDictNumerically(userSimilarityDict);
    }

    //filters out vectors that don't reach the minimum Similarity Ranking
    function filterMoviesMatchRanking(usersMoviesMatchRanking, filteredRatingVectorsDict, ratingVectorsDict, similarityFactor) {
        var minSimilarityRanking = Math.floor(usersMoviesMatchRanking[0][1] - (usersMoviesMatchRanking[0][1] * similarityFactor));
        var ranking;
        var tempUserIdentifier;

        for (ranking in usersMoviesMatchRanking) {
            if (usersMoviesMatchRanking[ranking][1] >= minSimilarityRanking) {
                tempUserIdentifier = usersMoviesMatchRanking[ranking][0];
                filteredRatingVectorsDict[tempUserIdentifier] = ratingVectorsDict[tempUserIdentifier];
            }
        }
    }

    //sorts users by the similarity of their ratings to the current user
    function generateUserSimilarityRanking(userVector, filteredRatingVectorsDict, resultUserRanking) {
        var userId;

        for (userId in filteredRatingVectorsDict) {
            resultUserRanking.push([userId, calcSimilarity(userVector, filteredRatingVectorsDict[userId])]);
        }
        sortListNumerically(resultUserRanking);
    }

    //returns the calculated euclidean distance between the vectors (ratings) of the current user (userVector)
    //and another user (compareVector)
    function calcSimilarity(userVector, compareVector) {
        // Formula: 1/(1+ sqrt(pow(userVector[key]-compareVector[key],2) + pow(...,2))
        var result;
        var sum = 0;
        var key;
        var x;
        var y;

        for (key in userVector) {

            x = userVector[key];
            if (compareVector[key]) {
                y = compareVector[key];
            }
            sum += Math.pow(x - y, 2);
        }

        result = 1 / (1 + Math.sqrt(sum));
        return result;
    }

    //returns a list of movies that the most similar user has upvoted but the current User didn't rate so far
    function addRecommendedMovies(userVector, ratingVectorsDict, resultUserRanking) {
        var result = [];
        var user;
        var movieId;

        //when movie has not been rated by the current user and the movie has been upvoted by a user with the most similar rating vector
        for (user in resultUserRanking) {
            //console.log(resultUserRanking[user]);
            for (movieId in ratingVectorsDict[resultUserRanking[user][0]]) {
                if (!userVector[movieId] && ratingVectorsDict[resultUserRanking[user][0]][movieId] === 1) {
                    result.push(movieId);
                }
            }
        }

        return _.unique(result);
    }

    //removes movies from recommendations when the current user has a recommended movie in one of his movielists
    function detachMoviesInListsFromRecommendations(resultRecommendations, successCallback) {
        var i;
        var index;

        firebaseRef.child("movielists").child(currentUserId).once("value", function (dataSnapshot) {
            dataSnapshot.forEach(function (childSnapshot) {
                var listData = _.values(childSnapshot.val());
                for (i = 0; i < listData.length; i++) {
                    if ($.inArray(listData[i], resultRecommendations) != -1) {
                        index = resultRecommendations.indexOf(listData[i]);
                        resultRecommendations.splice(index, 1);
                    }
                }
            });

            if (resultRecommendations.length > 0) {
                successCallback(resultRecommendations);
            } else {
                //console.log("No Recommended Results Error");
                onErrorCallback();
            }
        });

    }

    //returns a list from an input dictionary that is sorted from high to low values
    function sortDictNumerically(dict) {
        var userSimilarityRanking = [];
        var key;

        for (key in dict) {
            userSimilarityRanking.push([key, dict[key]]);
        }

        return sortListNumerically(userSimilarityRanking);
    }

    //returns a list from an input list that is sorted from high to low values
    function sortListNumerically(list) {
        list.sort(function (a, b) {
            return b[1] - a[1];
        });
        return list;
    }

    //retrieves the data that is stored under the ids of the recommended movie items
    function getMovieItemsFromFirebase(movieItems) {
        var result = [];
        var i = 0;
        getMovieItem(movieItems, i, result);
    }

    function getMovieItem(movieItems, i, result) {
        var temp = i;
        firebaseRef.child("movies").child(movieItems[i]).once("value", function (dataSnapshot) {
            currentMovieItems.push(dataSnapshot.val());
            result.push(dataSnapshot.val());
            if (i === (movieItems.length - 1)) {
                getMovieItemsFromFirebaseSuccessCallback(result);
                return;
            } else {
                temp++;
                getMovieItem(movieItems, temp, result);
            }
        });
    }

    function getMovieItemsFromFirebaseSuccessCallback(movieItems) {
        //console.log("RESULT");
        //console.log(movieItems);

        onGetRecommendationsSuccessCallback(movieItems);
    }

    function setOnErrorCallback(callback) {
        onErrorCallback = callback;
    }

    function setOnGetRecommendationsSuccessCallback(callback) {
        onGetRecommendationsSuccessCallback = callback;
    }

    that.setOnErrorCallback = setOnErrorCallback;
    that.setOnGetRecommendationsSuccessCallback = setOnGetRecommendationsSuccessCallback;
    that.getRecommendations = getRecommendations;
    return that;
};