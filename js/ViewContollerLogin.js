
/* global $ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.ViewControllerLogin = function (arguments) {
    var that = {},
        registerButton = arguments.registerButton,
        loginButton = arguments.loginButton;

    function init(){
        //console.log(registerButton,loginButton);
        $(document).ready(function () {
            $("#" + registerButton.id).leanModal({top: 160, overlay: 0.6, closeButton: ".modal_close"});
        });

        $("#" + loginButton.id).click(function (event) {
            event.preventDefault();
        });


        $(document).ready(function () {
            scaleVideoContainer();
            initBannerVideoSize(".video-container .poster img");
            initBannerVideoSize(".video-container .filter");
            initBannerVideoSize(".video-container video");

            $(window).on("resize", function () {
                scaleVideoContainer();
                scaleBannerVideoSize(".video-container .poster img");
                scaleBannerVideoSize(".video-container .filter");
                scaleBannerVideoSize(".video-container video");
            });
        });
    }

    function scaleVideoContainer() {
        var height = $(window).height() + 5;
        var unitHeight = parseInt(height) + "px";

        $(".homepage-video-module").css("height", unitHeight);
    }

    function initBannerVideoSize(element) {
        $(element).each(function () {
            $(this).data("height", $(this).height());
            $(this).data("width", $(this).width());
        });

        scaleBannerVideoSize(element);
    }

    function scaleBannerVideoSize(element) {
        var windowWidth = $(window).width(),
            windowHeight = $(window).height() + 5,
            videoWidth,
            videoHeight;

        $(element).each(function () {
            var videoAspectRatio = $(this).data("height") / $(this).data("width");

            $(this).width(windowWidth);

            if (windowWidth < 1000) {
                videoHeight = windowHeight;
                videoWidth = videoHeight / videoAspectRatio;
                $(this).css({"margin-top": 0, "margin-left": -(videoWidth - windowWidth) / 2 + "px"});

                $(this).width(videoWidth).height(videoHeight);
            }

            $(".homepage-video-module .video-container video").addClass("fadeIn animated");
        });
    }

    that.init = init;
    return that;
};
