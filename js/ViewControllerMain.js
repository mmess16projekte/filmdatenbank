/**
 * This module contains all functionality that can be experienced by the user in main.html
 * Because data is retrieved from Firebase some methods that are declared here have been passed on as
 * callbacks to the Firebase.js module.
 */

/* global $ _ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.ViewControllerMain = function (arguments) {
    var that = {},
        movieItemTemplate = arguments.movieItemTemplate,
        movieItemLocation = arguments.movieItemLocation,
        movieTopItemTemplate = arguments.movieItemTopTemplate,
        movieTopItemLocation = arguments.movieItemTopLocation,
        movieDataItemTemplate = arguments.movieItemDataTemplate,
        movieNavBarAddButton = arguments.movieNavBarAddButton,
        movieNavBarAddButtonImg = arguments.movieNavBarAddButtonImg,
        addPopup = arguments.addPopup,
        addPopupList = arguments.addPopupList,
        addPopupTitle = arguments.addPopupTitle,
        addPopupTextField = arguments.addPopupTextField,
        addPopupNewListButton = arguments.addPopupNewListButton,
        addPopupCloseButton = arguments.addPopupCloseButton,
        actionButton = arguments.actionButton,
        dramaButton = arguments.dramaButton,
        comedyButton = arguments.comedyButton,
        romanceButton = arguments.romanceButton,
        onUpvoteButtonClickedCallback,
        onDownvoteButtonClickedCallback,
        onAddButtonClickedCallback,
        addMovieToListCallback,
        movieItemCounter = 0,
        popupVisible,
        rowContainer,
        onScrollEndReachedCallback,
        movieItemsForAddingIds = [],
        movieItemsForAdding = [],
        alreadyVotedNodes = [],
        moviesAlreadyListed = [],
        allItemsLoaded,
        actionFilterActive = localStorage.getItem("actionFilterActive"),
        dramaFilterActive = localStorage.getItem("dramaFilterActive"),
        comedyFilterActive = localStorage.getItem("comedyFilterActive"),
        romanceFilterActive = localStorage.getItem("romanceFilterActive");


    //adds movieItems to the top
    function initTopView(topMovieItems) {
        var createEntryTemplate = _.template(movieTopItemTemplate);
        var movieItemTop;
        var i;
        var entryNode;

        for (i = 0; i < topMovieItems.length; i++) {

            entryNode = document.createElement("div");
            entryNode.innerHTML = createEntryTemplate(topMovieItems[i]);

            movieItemTop = MovieDatabaseApp.MovieItem();
            movieItemTop.setMovieId(topMovieItems[i].id);
            movieItemTop.setContainer(entryNode.querySelector("#top_container"));
            movieItemTop.setCancelButton(entryNode.querySelector("#cover_cancel_button_top"));
            movieItemTop.setCheckButton(entryNode.querySelector("#cover_check_button_top"));
            movieItemTop.setAddButton(entryNode.querySelector("#cover_add_button_top"));
            movieItemTop.setUpVoteButton(entryNode.querySelector("#cover_upvote_button_top"));
            movieItemTop.setDownVoteButton(entryNode.querySelector("#cover_downvote_button_top"));
            movieItemTop.setCoverImage(entryNode.querySelector("#cover_image_top"));
            movieItemTop.setAlreadyVoted(false);
            movieItemTop.setButtonsVisibleInMain(true);

            addTopListeners(movieItemTop, topMovieItems[i]);

            movieTopItemLocation.appendChild(entryNode);
        }
    }

    //add listeners to the four movie items that are displayed at the top
    function addTopListeners(movieItem, movieData) {
        var container = movieItem.getContainer();
        var addButtonTop = movieItem.getAddButton();
        var checkButtonTop = movieItem.getCheckButton();
        var cancelButtonTop = movieItem.getCancelButton();
        var upvoteButtonTop = movieItem.getUpVoteButton();
        var downvoteButtonTop = movieItem.getDownVoteButton();
        var coverImageTop = movieItem.getCoverImage();
        var movieId = movieItem.getMovieId();
        var indexOne;
        var indexTwo;

        addButtonTop.addEventListener("click", function () {
            //console.log("addButtonClicked");
            checkButtonTop.style.visibility = "visible";
            upvoteButtonTop.style.visibility = "hidden";
            downvoteButtonTop.style.visibility = "hidden";
            cancelButtonTop.style.visibility = "hidden";
            addButtonTop.style.visibility = "hidden";
            coverImageTop.style.opacity = 0.5;

            movieItem.setCancelButtonVisible(true);
            movieItem.setCheckButtonVisible(true);

            movieItemsForAddingIds.push(movieData.id);
            movieItemsForAdding.push(movieData);
            movieItemsForAddingIds = _.uniq(movieItemsForAddingIds);
            movieItemsForAdding = _.uniq(movieItemsForAdding);
            moviesAlreadyListed.push(movieItem);
            moviesAlreadyListed = _.uniq(moviesAlreadyListed);

            if (movieItemsForAddingIds.length != 0) {
                movieNavBarAddButton.style.visibility = "visible";
                movieNavBarAddButtonImg.webkitAnimationPlayState = "running";
            }
        });

        upvoteButtonTop.addEventListener("click", function () {
            hideButtons(addButtonTop, upvoteButtonTop, downvoteButtonTop, coverImageTop);
            onUpvoteButtonClickedCallback(movieId);
            movieItem.setAlreadyVoted(true);

        });

        downvoteButtonTop.addEventListener("click", function () {
            hideButtons(addButtonTop, upvoteButtonTop, downvoteButtonTop, coverImageTop);
            onDownvoteButtonClickedCallback(movieId);
            movieItem.setAlreadyVoted(true);
        });

        container.addEventListener("mouseover", function () {
            if (movieItem.getCheckButtonVisible()) {
                cancelButtonTop.style.visibility = "visible";
                checkButtonTop.style.visibility = "hidden";

            } else if (!movieItem.getAlreadyVoted() && movieItem.getButtonsVisibleInMain()) {
                showButtons(addButtonTop, upvoteButtonTop, downvoteButtonTop, coverImageTop);
            }
        });

        container.addEventListener("mouseout", function () {

            if (movieItem.getCancelButtonVisible()) {
                cancelButtonTop.style.visibility = "hidden";
                checkButtonTop.style.visibility = "visible";

            } else if (!movieItem.getAlreadyVoted() && movieItem.getButtonsVisibleInMain()) {
                hideButtons(addButtonTop, upvoteButtonTop, downvoteButtonTop, coverImageTop);
            }
        });

        coverImageTop.addEventListener("click", function () {
            initPopup(movieData);
        });

        cancelButtonTop.addEventListener("click", function () {
            cancelButtonTop.style.visibility = "hidden";
            checkButtonTop.style.visibility = "hidden";
            upvoteButtonTop.style.visibility = "visible";
            downvoteButtonTop.style.visibility = "visible";
            addButtonTop.style.visibility = "visible";
            coverImageTop.style.opacity = 1;
            indexOne = movieItemsForAddingIds.indexOf(movieData.id);
            indexTwo = movieItemsForAdding.indexOf(movieData);
            movieItemsForAddingIds.splice(indexOne, 1);
            movieItemsForAdding.splice(indexTwo, 1);
            movieItem.setCancelButtonVisible(false);
            movieItem.setCheckButtonVisible(false);

            if (movieItemsForAddingIds.length === 0) {
                movieNavBarAddButton.style.visibility = "hidden";
                movieNavBarAddButtonImg.webkitAnimationPlayState = "paused";
            }
        });
    }

    //adds movieItems to the bottom and adds additional rows if row is full
    function initBottomView(bottomMovieItems) {
        var createEntryTemplate = _.template(movieItemTemplate);
        var movieItem;
        var entryNode;
        var i;

        if (movieItemCounter % 6 == 0) {
            rowContainer = document.createElement("div");
            rowContainer.className = "row";
            movieItemLocation.appendChild(rowContainer);
        }

        for (i = 0; i < bottomMovieItems.length; i++) {
            entryNode = document.createElement("div");
            entryNode.innerHTML = createEntryTemplate(bottomMovieItems[i]);


            movieItem = MovieDatabaseApp.MovieItem();

            movieItem.setMovieId(bottomMovieItems[i].id);
            movieItem.setContainer(entryNode.querySelector("#bottom_container"));
            movieItem.setCancelButton(entryNode.querySelector("#cover_cancel_button"));
            movieItem.setCheckButton(entryNode.querySelector("#cover_check_button"));
            movieItem.setAddButton(entryNode.querySelector("#cover_add_button"));
            movieItem.setUpVoteButton(entryNode.querySelector("#cover_upvote_button"));
            movieItem.setDownVoteButton(entryNode.querySelector("#cover_downvote_button"));
            movieItem.setCoverImage(entryNode.querySelector("#cover_image"));
            movieItem.setAlreadyVoted(false);
            movieItem.setButtonsVisibleInMain(true);

            addBottomListeners(movieItem, bottomMovieItems[i]);

            if ($("#" + bottomMovieItems[0].id).length === 0) {
                movieItemCounter++;
                rowContainer.appendChild(entryNode);
            }
        }


    }

    //add listeners to movie items that are displayed in the bottom rows
    function addBottomListeners(movieItem, movieData) {
        var container = movieItem.getContainer();
        var addButton = movieItem.getAddButton();
        var checkButton = movieItem.getCheckButton();
        var cancelButton = movieItem.getCancelButton();
        var upvoteButton = movieItem.getUpVoteButton();
        var downvoteButton = movieItem.getDownVoteButton();
        var coverImage = movieItem.getCoverImage();
        var movieId = movieItem.getMovieId();
        var indexOne;
        var indexTwo;

        addButton.addEventListener("click", function () {
            //console.log("addButtonClicked");
            checkButton.style.visibility = "visible";
            upvoteButton.style.visibility = "hidden";
            downvoteButton.style.visibility = "hidden";
            cancelButton.style.visibility = "hidden";
            addButton.style.visibility = "hidden";
            coverImage.style.opacity = 0.5;

            movieItem.setCancelButtonVisible(true);
            movieItem.setCheckButtonVisible(true);

            movieItemsForAddingIds.push(movieData.id);
            movieItemsForAdding.push(movieData);
            movieItemsForAddingIds = _.uniq(movieItemsForAddingIds);
            movieItemsForAdding = _.uniq(movieItemsForAdding);
            moviesAlreadyListed.push(movieItem);
            moviesAlreadyListed = _.uniq(moviesAlreadyListed);

            if (movieItemsForAddingIds.length != 0) {
                movieNavBarAddButton.style.visibility = "visible";
                movieNavBarAddButtonImg.webkitAnimationPlayState = "running";
            }
        });

        upvoteButton.addEventListener("click", function () {
            hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            movieItem.setAlreadyVoted(true);
            alreadyVotedNodes.push(movieItem);
            onUpvoteButtonClickedCallback(movieId);
        });

        downvoteButton.addEventListener("click", function () {
            hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            movieItem.setAlreadyVoted(true);
            alreadyVotedNodes.push(movieItem);
            onDownvoteButtonClickedCallback(movieId);
        });

        container.addEventListener("mouseover", function () {
            if (movieItem.getCheckButtonVisible()) {
                cancelButton.style.visibility = "visible";
                checkButton.style.visibility = "hidden";

            } else if (!movieItem.getAlreadyVoted() && movieItem.getButtonsVisibleInMain()) {
                showButtons(addButton, upvoteButton, downvoteButton, coverImage);
            }
        });

        container.addEventListener("mouseout", function () {
            if (movieItem.getCancelButtonVisible()) {
                cancelButton.style.visibility = "hidden";
                checkButton.style.visibility = "visible";

            } else if (!movieItem.getAlreadyVoted() && movieItem.getButtonsVisibleInMain()) {
                hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            }
        });

        coverImage.addEventListener("click", function () {
            initPopup(movieData);
        });

        cancelButton.addEventListener("click", function () {
            cancelButton.style.visibility = "hidden";
            checkButton.style.visibility = "hidden";
            upvoteButton.style.visibility = "visible";
            downvoteButton.style.visibility = "visible";
            addButton.style.visibility = "visible";
            coverImage.style.opacity = 1;
            indexOne = movieItemsForAddingIds.indexOf(movieData.id);
            indexTwo = movieItemsForAdding.indexOf(movieData);
            movieItemsForAddingIds.splice(indexOne, 1);
            movieItemsForAdding.splice(indexTwo, 1);
            movieItem.setCancelButtonVisible(false);
            movieItem.setCheckButtonVisible(false);

            if (movieItemsForAddingIds.length === 0) {
                movieNavBarAddButton.style.visibility = "hidden";
                movieNavBarAddButtonImg.webkitAnimationPlayState = "paused";
            }
        });
    }

    //display and add logic to popup with movie data
    function initPopup(movieData) {
        var createEntryTemplate;
        var entryNode;
        var amazonButton;
        popupVisible = true;

        createEntryTemplate = _.template(movieDataItemTemplate);
        entryNode = document.createElement("div");
        entryNode.innerHTML = createEntryTemplate(movieData);

        //Amazon Button logic
        amazonButton = entryNode.querySelector("#amazon_button");
        amazonButton.addEventListener("click", function () {
            var url = "https://www.amazon.de/s/ref=nb_sb_noss_2?__mk_de_DE=ÅMÅŽÕÑ&url=" +
                "search-alias%3Ddvd&field-keywords=" + movieData.title;
            window.open(url);
        });

        rowContainer.appendChild(entryNode);

        entryNode.querySelector("#screen").addEventListener("click", function () {
            $("#overlay").fadeOut();
            $(".dialog").fadeOut();
            rowContainer.removeChild(entryNode);
            popupVisible = false;
        });

        $("#overlay").fadeIn();
        $("#dialog-data").fadeIn();
    }

    //adds the results from Algorithm to the top view until it is full
    //and starts filling up bottom view until all results are displayed
    function initRecommendations(recommendedMovieItems) {
        var i;
        var ii;

        for (i = 0; i < recommendedMovieItems.length && i < 4; i++) {
            initTopView([recommendedMovieItems[i]]);
        }
        for (ii = 4; ii < recommendedMovieItems.length; ii++) {
            initBottomView([recommendedMovieItems[ii]]);
        }
    }

    //add listener to (blinking) Add Button in Nav Bar
    function addMovieNavBarAddButtonListener() {
        movieNavBarAddButton.addEventListener("click", function () {
            //console.log("Add Popup Opens");
            $("#" + addPopup.id).modal("show");
            onAddButtonClickedCallback();
            initAddPopupTitle();
            addPopupList.innerHTML = "";

            $(".modal-wide").on("show.bs.modal", function () {
                var height = $(window).height() - 200;
                $(this).find(".modal-body").css("max-height", height);
            });

        });
    }

    //add listeners to filter Buttons
    function addFilterListeners() {
        //console.log("Action  " + actionFilterActive);
        //console.log("Drama  " + dramaFilterActive);
        //console.log("Comedy  " + comedyFilterActive);
        //console.log("Romance  " + romanceFilterActive);

        var actionButtonText = document.getElementById("action-nav-button");
        var dramaButtonText = document.getElementById("drama-nav-button");
        var comedyButtonText = document.getElementById("comedy-nav-button");
        var romanceButtonText = document.getElementById("romance-nav-button");

        if (actionFilterActive === "true") {
            actionButtonText.style.color = "#00FF00";
        }
        if (dramaFilterActive === "true") {
            dramaButtonText.style.color = "#00FF00";
        }
        if (comedyFilterActive === "true") {
            comedyButtonText.style.color = "#00FF00";
        }
        if (romanceFilterActive === "true") {
            romanceButtonText.style.color = "#00FF00";
        }


        actionButton.addEventListener("click", function () {
            //console.log("Action Button Clicked");
            if (actionFilterActive === "false") {
                localStorage.setItem("actionFilterActive", true);
            } else {
                localStorage.setItem("actionFilterActive", false);
            }
            location.reload(true);
        });

        dramaButton.addEventListener("click", function () {
            //console.log("Drama Button Clicked");
            if (dramaFilterActive === "false") {
                localStorage.setItem("dramaFilterActive", true);
            } else {
                localStorage.setItem("dramaFilterActive", false);

            }
            location.reload(true);
        });

        comedyButton.addEventListener("click", function () {
            //console.log("Comedy Button Clicked");
            if (comedyFilterActive === "false") {
                localStorage.setItem("comedyFilterActive", true);
            } else {
                localStorage.setItem("comedyFilterActive", false);
            }
            location.reload(true);
        });

        romanceButton.addEventListener("click", function () {
            //console.log("Romance Button Clicked");
            if (romanceFilterActive === "false") {
                localStorage.setItem("romanceFilterActive", true);
            } else {
                localStorage.setItem("romanceFilterActive", false);
            }
            location.reload(true);
        });
    }

    //add Listeners to AddPopup
    function addAddPopupListeners() {
        addPopupNewListButton.addEventListener("click", function () {
            if (addPopupTextField.value === "") {
                window.alert("Please enter a list name first!");
            } else {
                addMovieListEntryToPopupCallback(addPopupTextField.value);
                addMovieToListCallback(movieItemsForAddingIds, addPopupTextField.value);
                removeAllMarkedMoviesButtons(moviesAlreadyListed);
                $("#" + addPopup.id).modal("hide");
                addPopupTextField.value = "";
            }
        });

        addPopupCloseButton.addEventListener("click", function () {
            //console.log("Close Button pressed");
        });
    }

    //adds existing movielists from current user to addPopup
    function addMovieListEntryToPopupCallback(movieListName) {
        var list = addPopupList;
        var entries = list.childNodes;
        var movieListNames = [];
        var i;
        var newLI;

        for (i = 0; i < entries.length; i++) {
            movieListNames.push(entries[i].innerHTML);
        }

        if (!movieListNames.includes(movieListName)) {
            newLI = document.createElement("li");
            newLI.innerHTML = movieListName;

            newLI.addEventListener("click", function () {
                addMovieToListCallback(movieItemsForAddingIds, movieListName);
                removeAllMarkedMoviesButtons(moviesAlreadyListed);
                $("#" + addPopup.id).modal("hide");
            });
            list.appendChild(newLI);
            setTimeout(function () {
                newLI.className = newLI.className + " movie_list";
            }, 10);
        } else {
            window.alert("This movie list already exists");
        }
    }

    //add all titles from movieItems that have been marked to the AddPopup title
    function initAddPopupTitle() {
        var info = "";
        var item;

        for (item in movieItemsForAdding) {
            //console.log(movieItemsForAdding[item]);
            info += "\"" + movieItemsForAdding[item].title + "\"," + "<br>";
        }

        addPopupTitle.innerHTML = info;
    }

    //resets all marked items after they have been added to a list
    function removeAllMarkedMoviesButtons(moviesAlreadyListed) {
        var i;

        movieItemsForAdding = [];
        movieItemsForAddingIds = [];
        movieNavBarAddButton.style.visibility = "hidden";
        movieNavBarAddButtonImg.webkitAnimationPlayState = "paused";

        for (i = 0; i < moviesAlreadyListed.length; i++) {
            moviesAlreadyListed[i].getCancelButton().style.visibility = "hidden";
            moviesAlreadyListed[i].getCheckButton().style.visibility = "hidden";
            moviesAlreadyListed[i].getCoverImage().style.opacity = 0.5;
            moviesAlreadyListed[i].getCoverImage().style.webkitFilter = "drop-shadow(0px 0px 0px rgba(255,255,255, 1)) brightness(100%)";
            moviesAlreadyListed[i].setButtonsVisibleInMain(false);
            moviesAlreadyListed[i].setCancelButtonVisible(false);
            moviesAlreadyListed[i].setCheckButtonVisible(false);
        }
    }

    //show buttons on movie item cover
    function showButtons(addButton, upvoteButton, downvoteButton, coverImage) {
        coverImage.style.webkitFilter = "drop-shadow(0px 0px 10px rgba(255,255,255,.8)) brightness(100%)";

        addButton.style.visibility = "visible";
        addButton.style.opacity = "0.8";
        upvoteButton.style.visibility = "visible";
        upvoteButton.style.opacity = "0.8";
        downvoteButton.style.visibility = "visible";
        downvoteButton.style.opacity = "0.8";
    }

    //hide buttons on movie item cover
    function hideButtons(addButton, upvoteButton, downvoteButton, coverImage) {
        coverImage.style.webkitFilter = "drop-shadow(0px 0px 0px rgba(255,255,255, 1)) brightness(100%)";

        addButton.style.visibility = "hidden";
        upvoteButton.style.visibility = "hidden";
        downvoteButton.style.visibility = "hidden";
    }

    //loads additional movie items when the bottom of the page is reached
    function addOnScrollEndReachedListener() {
        var timerStart = 0;
        var firstDetection = true;

        window.onscroll = function () {
            if ((window.innerHeight + window.scrollY) >= document.body.scrollHeight && !popupVisible) {
                if (allItemsLoaded) {
                    allItemsLoaded = false;
                    //console.log("You can scroll now");
                    onScrollEndReachedCallback();
                    if (firstDetection) {
                        //console.log("Bottom of page");
                        timerStart = Date.now();
                        firstDetection = false;
                    } else {
                        if (Date.now() - timerStart > 5000) {
                            firstDetection = true;
                            //console.log("ACTIVE AGAIN");
                        }
                    }
                }
            }
        };


    }

    function setOnScrollEndReachedCallback(callback) {
        onScrollEndReachedCallback = callback;
    }

    function setOnAddButtonClickedCallback(callback) {
        onAddButtonClickedCallback = callback;
    }

    function setOnUpvoteButtonClickedCallback(callback) {
        onUpvoteButtonClickedCallback = callback;
    }

    function setOnDownvoteButtonClickedCallback(callback) {
        onDownvoteButtonClickedCallback = callback;
    }

    function setOnAddButtonTopClickedCallback(callback) {
        onAddButtonClickedCallback = callback;
    }

    function setOnUpvoteButtonTopClickedCallback(callback) {
        onUpvoteButtonClickedCallback = callback;
    }

    function setOnDownvoteButtonTopClickedCallback(callback) {
        onDownvoteButtonClickedCallback = callback;
    }

    function setAddMovieToListCallback(callback) {
        addMovieToListCallback = callback;
    }

    function setOnScrollEndReachedFlag(value) {
        allItemsLoaded = value;
    }


    that.addAddPopupListeners = addAddPopupListeners;
    that.addFilterListeners = addFilterListeners;
    that.setOnScrollEndReachedFlag = setOnScrollEndReachedFlag;
    that.initRecommendations = initRecommendations;
    that.setAddMovieToListCallback = setAddMovieToListCallback;
    that.addMovieListEntryToPopupCallback = addMovieListEntryToPopupCallback;
    that.addMovieNavBarAddButtonListener = addMovieNavBarAddButtonListener;
    that.addOnScrollEndReachedListener = addOnScrollEndReachedListener;
    that.setOnScrollEndReachedCallback = setOnScrollEndReachedCallback;
    that.setOnDownvoteButtonClickedCallback = setOnDownvoteButtonClickedCallback;
    that.setOnAddButtonClickedCallback = setOnAddButtonClickedCallback;
    that.setOnUpvoteButtonClickedCallback = setOnUpvoteButtonClickedCallback;
    that.setOnDownvoteButtonTopClickedCallback = setOnDownvoteButtonTopClickedCallback;
    that.setOnAddButtonTopClickedCallback = setOnAddButtonTopClickedCallback;
    that.setOnUpvoteButtonTopClickedCallback = setOnUpvoteButtonTopClickedCallback;
    that.initTopView = initTopView;
    that.initBottomView = initBottomView;
    return that;
};