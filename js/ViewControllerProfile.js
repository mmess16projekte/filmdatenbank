/**
 * This module contains all functionality that can be experienced by the user in profile.html
 * Because data is retrieved from Firebase some methods that are declared here have been passed on as
 * callbacks to the Firebase.js module.
 */

/* global $ _ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.ViewControllerProfile = function (arguments) {
    var that = {},
        rowItemTemplate = arguments.rowItemTemplate,
        rowItemLocation = arguments.rowItemLocation,
        movieItemTemplate = arguments.movieItemTemplate,
        movieDataItemTemplate = arguments.movieItemDataTemplate,
        wasteBinNavBarButton = arguments.wasteBinNavBarButton,
        wasteBinNavBarButtonImg = arguments.wasteBinNavBarButtonImg,
        greetingNameField = arguments.greetingNameField,
        getMovieListItemsCallback,
        popupVisible,
        rowContainer,
        movieItemsForRemoval = [],
        alreadyVotedNodes = [],
        allDisplayedMovieItems = [],
        rowCounter = 0,
        onUpvoteButtonClickedCallback,
        onDownvoteButtonClickedCallback,
        onMovieItemsRemovedCallback,
        onMovieItemRemovedFromAllListsCallback;


    //add logic (delete all marked movies) to delete button in the navigation bar
    function addNavBarDeleteButtonListener() {
        wasteBinNavBarButton.addEventListener("click", function () {
            //console.log("Movie Nav Bar Add Button Clicked");
            var i;
            wasteBinNavBarButton.style.visibility = "hidden";
            wasteBinNavBarButtonImg.webkitAnimationPlayState = "paused";
            allDisplayedMovieItems = _.difference(allDisplayedMovieItems, movieItemsForRemoval);

            for (i = 0; i < movieItemsForRemoval.length; i++) {
                onMovieItemsRemovedCallback(movieItemsForRemoval[i].getMovieId().toString(), movieItemsForRemoval[i].getListName());
            }
            removeItemsFromList(movieItemsForRemoval);

            movieItemsForRemoval = [];
        });

    }

    //executed when the movielists are retrieved from Firebase
    function init(movieListName, movieListContent) {
        var movieListItemIds = [];
        var key;

        for (key in movieListContent) {
            movieListItemIds.push(movieListContent[key]);

        }
        getMovieListItemsCallback(movieListName, movieListItemIds);

    }

    //displays a movielist with name and the corresponding movie items
    function initRow(listName, movieItems) {
        var temp = {};
        var createEntryTemplate;
        var rowNode;
        var markAllButton;
        var markAllButtonImg;
        //var entryNode;
        var index;
        //var movieItem;
        var i;
        var x;
        var y;

        temp.rowId = rowCounter;
        temp.listName = listName;


        createEntryTemplate = _.template(rowItemTemplate);

        rowNode = document.createElement("li");
        rowContainer = rowNode;
        rowNode.innerHTML = createEntryTemplate(temp);
        rowNode.style.listStyleType = "none";

        rowItemLocation.appendChild(rowNode);


        markAllButton = rowNode.querySelector("#mark_all_button");
        markAllButtonImg = rowNode.querySelector("#mark_all_button_img");


        for (i = 0; i < movieItems.length; i++) {
            createEntryTemplate = _.template(movieItemTemplate);
            entryNode = document.createElement("div");
            entryNode.className = "tile";
            entryNode.id = movieItems[i].id.toString();
            entryNode.innerHTML = createEntryTemplate(movieItems[i]);

            movieItem = new MovieDatabaseApp.MovieItem();
            movieItem.setMovieId(movieItems[i].id);
            movieItem.setContainer(entryNode.querySelector("#bottom_container"));
            movieItem.setCoverImage(entryNode.querySelector("#cover_image"));
            movieItem.setAddButton(entryNode.querySelector("#cover_add_button"));
            movieItem.setCheckButton(entryNode.querySelector("#cover_check_button"));
            movieItem.setCancelButton(entryNode.querySelector("#cover_cancel_button"));
            movieItem.setUpVoteButton(entryNode.querySelector("#cover_upvote_button"));
            movieItem.setDownVoteButton(entryNode.querySelector("#cover_downvote_button"));
            movieItem.setParentNode(rowNode.querySelector(".row__inner"));
            movieItem.setTopNode(entryNode);
            movieItem.setAlreadyVoted(false);
            movieItem.setListName(listName);
            movieItem.setMarkAllButtonImg(markAllButtonImg);

            allDisplayedMovieItems.push(movieItem);
            allDisplayedMovieItems = _.uniq(allDisplayedMovieItems);


            addListeners(movieItem, movieItems[i]);

            movieItem.getParentNode().appendChild(entryNode);
        }

        markAllButton.addEventListener("click", function () {

            // deactivate marks
            if (markAllButtonImg.src.includes("activated")) {

                markAllButtonImg.src = "img/buttons/mark_all_neutral.png";

                for (x = 0; x < allDisplayedMovieItems.length; x++) {

                    if (allDisplayedMovieItems[x].getListName() === listName) {

                        allDisplayedMovieItems[x].getCancelButton().click();
                        index = movieItemsForRemoval.indexOf(allDisplayedMovieItems[i]);
                        movieItemsForRemoval.splice(index, 1);
                    }
                }

                if (movieItemsForRemoval.length === 0) {
                    wasteBinNavBarButton.style.visibility = "hidden";
                    wasteBinNavBarButtonImg.webkitAnimationPlayState = "paused";
                }

                // activate all
            } else {

                if (movieItemsForRemoval.length === 0) {
                    wasteBinNavBarButton.style.visibility = "visible";
                    wasteBinNavBarButtonImg.webkitAnimationPlayState = "running";
                }

                markAllButtonImg.src = "img/buttons/mark_all_activated.png";
                for (y = 0; y < allDisplayedMovieItems.length; y++) {


                    if (allDisplayedMovieItems[y].getListName() === listName) {
                        allDisplayedMovieItems[y].getAddButton().click();
                        movieItemsForRemoval.push(allDisplayedMovieItems[i]);
                        movieItemsForRemoval = _.uniq(movieItemsForRemoval);
                    }
                }

            }

        });


        rowCounter++;
    }

    //add listeners to every movie cover
    function addListeners(movieItem, movieData) {
        var i;
        var container = movieItem.getContainer();
        var addButton = movieItem.getAddButton();
        var checkButton = movieItem.getCheckButton();
        var cancelButton = movieItem.getCancelButton();
        var upvoteButton = movieItem.getUpVoteButton();
        var downvoteButton = movieItem.getDownVoteButton();
        var coverImage = movieItem.getCoverImage();
        var indexOne;

        addButton.addEventListener("click", function () {
            //console.log("addButtonClicked");
            var allMarkedItemsInList = [];
            for (i = 0; i < allDisplayedMovieItems.length; i++) {
                if (allDisplayedMovieItems[i].getListName() === movieItem.getListName()) {
                    allMarkedItemsInList.push(allDisplayedMovieItems[i]);
                }
            }

            checkButton.style.visibility = "visible";
            upvoteButton.style.visibility = "hidden";
            downvoteButton.style.visibility = "hidden";
            cancelButton.style.visibility = "hidden";
            addButton.style.visibility = "hidden";
            coverImage.style.opacity = 0.5;
            movieItem.setCancelButtonVisible(true);
            movieItem.setCheckButtonVisible(true);

            movieItemsForRemoval.push(movieItem);
            movieItemsForRemoval = _.uniq(movieItemsForRemoval);

            if (_.difference(allMarkedItemsInList, movieItemsForRemoval).length === 0) {
                movieItem.getMarkAllButtonImg().src = "img/buttons/mark_all_activated.png";
            }

            if (movieItemsForRemoval.length != 0) {
                wasteBinNavBarButton.style.visibility = "visible";
                wasteBinNavBarButtonImg.webkitAnimationPlayState = "running";
            }
        });


        upvoteButton.addEventListener("click", function () {
            var i;
            hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            movieItem.setAlreadyVoted(true);
            alreadyVotedNodes.push(movieItem);
            onUpvoteButtonClickedCallback(movieItem.getMovieId());
            onMovieItemRemovedFromAllListsCallback([movieData.id.toString()]);

            for (i = 0; i < allDisplayedMovieItems.length; i++) {
                if (allDisplayedMovieItems[i].getMovieId() === movieItem.getMovieId()) {
                    removeItemsFromList([allDisplayedMovieItems[i]]);
                }
            }
        });

        downvoteButton.addEventListener("click", function () {
            var i;
            hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            movieItem.setAlreadyVoted(true);
            alreadyVotedNodes.push(movieItem);
            onDownvoteButtonClickedCallback(movieItem.getMovieId());
            onMovieItemRemovedFromAllListsCallback([movieData.id.toString()]);
            for (i = 0; i < allDisplayedMovieItems.length; i++) {
                if (allDisplayedMovieItems[i].getMovieId() === movieItem.getMovieId()) {
                    removeItemsFromList([allDisplayedMovieItems[i]]);
                }
            }
        });

        container.addEventListener("mouseover", function () {
            if (movieItem.getCheckButtonVisible()) {
                cancelButton.style.visibility = "visible";
                checkButton.style.visibility = "hidden";

            } else if (!movieItem.getAlreadyVoted()) {
                showButtons(addButton, upvoteButton, downvoteButton, coverImage);
            }
        });

        container.addEventListener("mouseout", function () {
            if (movieItem.getCancelButtonVisible()) {
                cancelButton.style.visibility = "hidden";
                checkButton.style.visibility = "visible";

            } else if (!movieItem.getAlreadyVoted()) {
                hideButtons(addButton, upvoteButton, downvoteButton, coverImage);
            }
        });

        coverImage.addEventListener("click", function () {
            initPopup(movieData);
        });

        cancelButton.addEventListener("click", function () {
            //console.log("Cancel Button Clicked");
            movieItem.getMarkAllButtonImg().src = "img/buttons/mark_all_neutral.png";

            cancelButton.style.visibility = "hidden";
            checkButton.style.visibility = "hidden";
            upvoteButton.style.visibility = "visible";
            downvoteButton.style.visibility = "visible";
            addButton.style.visibility = "visible";
            coverImage.style.opacity = 1;
            movieItem.setCancelButtonVisible(false);
            movieItem.setCheckButtonVisible(false);

            indexOne = movieItemsForRemoval.indexOf(movieItem);
            movieItemsForRemoval.splice(indexOne, 1);

            if (movieItemsForRemoval.length === 0) {

                wasteBinNavBarButton.style.visibility = "hidden";
                wasteBinNavBarButtonImg.webkitAnimationPlayState = "paused";

            }
        });
    }


    //inits a popup which displays the data of a specific movie
    function initPopup(movieData) {
        var createEntryTemplate = _.template(movieDataItemTemplate);
        var entryNode = document.createElement("div");
        var amazonButton;

        popupVisible = true;
        entryNode.innerHTML = createEntryTemplate(movieData);

        //Amazon Button logic
        amazonButton = entryNode.querySelector("#amazon_button");
        amazonButton.addEventListener("click", function () {
            var url = "https://www.amazon.de/s/ref=nb_sb_noss_2?__mk_de_DE=ÅMÅŽÕÑ&url=search-alias%3Ddvd&field-keywords=" + movieData.title;
            window.open(url);
        });

        rowContainer.appendChild(entryNode);

        entryNode.querySelector("#screen").addEventListener("click", function () {
            $("#overlay").fadeOut();
            $(".dialog").fadeOut();
            rowContainer.removeChild(entryNode);
            popupVisible = false;
        });

        $("#overlay").fadeIn();
        $("#dialog-data").fadeIn();
    }

    //removes a list of movieItems from the view
    function removeItemsFromList(movieItems) {
        var node;
        var i;
        var parent;
        var row;
        var temp = _.unique(movieItems);

        for (i = 0; i < temp.length; i++) {
            node = temp[i].getContainer().parentElement;
            parent = node.parentElement;

            parent.removeChild(node);

            if (parent.children.length === 0) {
                row = parent.parentElement.parentElement;
                row.parentElement.removeChild(row);
            }
        }
    }

    //show buttons on movie item cover
    function showButtons(addButton, upvoteButton, downvoteButton) {
        addButton.style.visibility = "visible";
        addButton.style.opacity = "0.8";
        upvoteButton.style.visibility = "visible";
        upvoteButton.style.opacity = "0.8";
        downvoteButton.style.visibility = "visible";
        downvoteButton.style.opacity = "0.8";
    }

    //hide buttons on movie item cover
    function hideButtons(addButton, upvoteButton, downvoteButton) {
        addButton.style.visibility = "hidden";
        upvoteButton.style.visibility = "hidden";
        downvoteButton.style.visibility = "hidden";
    }

    function initGreetingsNameField(username) {
        $("#" + greetingNameField.id).text("Hello " + username + ",");
    }

    function setGetMovieListItemsCallback(callback) {
        getMovieListItemsCallback = callback;
    }

    function setOnDownvoteButtonClickedCallback(callback) {
        onDownvoteButtonClickedCallback = callback;
    }

    function setOnUpvoteButtonClickedCallbacK(callback) {
        onUpvoteButtonClickedCallback = callback;
    }

    function setOnMovieItemRemovedFromAllListsCallback(callback) {
        onMovieItemRemovedFromAllListsCallback = callback;
    }

    function setOnMovieItemsRemovedCallback(callback) {
        onMovieItemsRemovedCallback = callback;
    }



    that.setOnMovieItemRemovedFromAllListsCallback = setOnMovieItemRemovedFromAllListsCallback;
    that.initGreetingsNameField = initGreetingsNameField;
    that.addNavBarDeleteButtonListener = addNavBarDeleteButtonListener;
    that.setOnMovieItemsRemovedCallback = setOnMovieItemsRemovedCallback;
    that.setOnDownvoteButtonClickedCallback = setOnDownvoteButtonClickedCallback;
    that.setOnUpvoteButtonClickedCallbacK = setOnUpvoteButtonClickedCallbacK;
    that.initRow = initRow;
    that.setGetMovieListItemsCallback = setGetMovieListItemsCallback;
    that.init = init;
    return that;

};