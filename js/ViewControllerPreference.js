/**
 * This module contains all functionality that can be experienced by the user in preference.html
 * Because data is retrieved from Firebase some methods that are declared here have been passed on as
 * callbacks to the Firebase.js module.
 */

/* global $ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.ViewControllerPreference = function (arguments) {
    var that = {},
        infoContainer = arguments.infoContainer,
        okButton = arguments.okButton,
        backgroundImg = arguments.backgroundImg,
        poster = arguments.poster,
        likeButton = arguments.likeButton,
        nextButton = arguments.nextButton,
        dislikeButton = arguments.dislikeButton,
        onNextButtonClickedCallback,
        onLikeButtonClickedCallback,
        onDislikeButtonClickedCallback,
        movieItems = [],
        ratingsCounter = 0;


    //changes the view to accustom the current movie item
    function initView() {
        backgroundImg.src = movieItems[0].poster_path;
        poster.src = movieItems[0].poster_path;
    }

    //adds item to list which will be used to display the current movie information
    function addItemsToList(data) {
        //console.log("ADD ITEMS TO LIST");
        movieItems.push(data);
    }

    //add listeners to all buttons visible in preference.html (includes the ok button from info screen)
    function addListeners() {
        $("#" + okButton.id).click(function () {
            // Animate the webpage's background, causing it to move
            $("#" + infoContainer.id).animate({
                top: "-=2000px"
            }, 800);
        });

        nextButton.addEventListener("click", function () {
            //console.log("Next Button Clicked");
            nextButtonClicked();
        });

        likeButton.addEventListener("click", function () {
            //console.log("Like Button Clicked");
            onLikeButtonClickedCallback(movieItems[0].id);
            nextButtonClicked();
        });

        dislikeButton.addEventListener("click", function () {
            //console.log("Dislike Button Clicked");
            onDislikeButtonClickedCallback(movieItems[0].id);
            nextButtonClicked();
        });
    }

    //when the user has rated 10 movies he will be sent to main.html
    function processRatingsCallback() {
        ratingsCounter++;
        if (ratingsCounter >= 10) {
            parent.location = "main.html";
        }
    }

    //displays next movie item
    function nextButtonClicked() {
        movieItems.splice(0, 1);
        onNextButtonClickedCallback();
        initView();
    }

    function setOnNextButtonClickedCallback(callback) {
        onNextButtonClickedCallback = callback;
    }

    function setOnDislikeButtonClickedCallback(callback) {
        onDislikeButtonClickedCallback = callback;
    }

    function setOnLikeButtonClickedCallback(callback) {
        onLikeButtonClickedCallback = callback;
    }


    that.processRatingsCallback = processRatingsCallback;
    that.setOnLikeButtonClickedCallback = setOnLikeButtonClickedCallback;
    that.setOnDislikeButtonClickedCallback = setOnDislikeButtonClickedCallback;
    that.addItemsToList = addItemsToList;
    that.setOnNextButtonClickedCallback = setOnNextButtonClickedCallback;
    that.addListeners = addListeners;
    that.initView = initView;
    return that;
};