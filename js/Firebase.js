/**
 * This module contains the functionality associated with processing data over firebase,
 * such as saving and retrieving data. User login and registration via firebase is handled in
 * FirebaseLogin.js and FirebaseRegistration.js
 */

/* global Firebase _ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.Firebase = function () {
    var that = {},
        movieItemRequestLimitMain = 18 + 4,
        recommendedMovieItemsLength = 0,
        movieItemsRequestedCounter = 0,
        movieItemRequestLimitPreference = 2,
        currentUserId = localStorage.getItem("userId"),
        firebaseRef,
        initTopViewCallback,
        initBottomViewCallback,
        processMoviesForPreferencesCallback,
        onMovieListsFromCurrentUserRetrievedCallback,
        onMovieRatingsRetrievedCallback,
        onAllMovieItemsLoadedCallback,
        onMovieItemsRetrievedCallback,
        onMovieItemsFromListRetrievedCallback,
        preferenceRatingCallback,
        getCurrentUserNameCallback,
        lastRetrievedMovieItemTimestampOne,
        lastRetrievedMovieItemTimestampTwo,
        onComplete,
        //currentOperation,
        excludedMovieIds = [],
        firstInit = true;


    function init() {
        firebaseRef = new Firebase("https://filmdatenbank.firebaseio.com");
    }

    //returns the currently loged in user id
    function getCurrentUserId() {
        return currentUserId;
    }

    //returns the firebase reference
    function getFirebaseRef() {
        return firebaseRef;
    }

    //get all movies that have been voted by the current user
    function getUserRatings() {
        getDownvotedMovies(getUpvotedMovies, getVotedMoviesCallback);
    }

    //gets called when all movie ratings have been retrieved from firebase and joined
    function getVotedMoviesCallback(ratings) {
        //console.log("GET MOVIES CALLBACK EXECUTED");
        onMovieRatingsRetrievedCallback(ratings);
    }

    //get all movies that have been upvoted by the current user
    function getUpvotedMovies(tempResult, callback) {
        firebaseRef.child("movieRatings").child("upvote___" + currentUserId).once("value", function (dataSnapshot) {
            callback(tempResult.concat(_.values(dataSnapshot.val())));
        });
    }

    //get all movies that have been downvoted by the current user
    function getDownvotedMovies(callback, passingCallback) {
        firebaseRef.child("movieRatings").child("downvote___" + currentUserId).once("value", function (dataSnapshot) {
            callback(_.values(dataSnapshot.val()), passingCallback);
        });
    }

    //checks for movies that are currently stored in movielists or have been rated before by the current user
    function checkForExcludedMovies(callback) {
        var onExecuted;
        var key;
        var entry;
        var i;

        firebaseRef.child("movielists").child(currentUserId).once("value", function (dataSnapshot) {
            //console.log(_.values(dataSnapshot.val()));
            for (key in dataSnapshot.val()) {
                for (entry in dataSnapshot.val()[key]) {
                    excludedMovieIds.push(dataSnapshot.val()[key][entry]);
                }
            }
            getDownvotedMovies(getUpvotedMovies, onExecuted);
        });

        onExecuted = function (ratings) {
            //console.log("onExecuted");
            //console.log(ratings);

            var formattedRatings = [];
            for (i = 0; i < ratings.length; i++) {
                formattedRatings.push(ratings[i].toString());
            }
            excludedMovieIds = excludedMovieIds.concat(formattedRatings);
            excludedMovieIds = _.uniq(excludedMovieIds);
            callback(excludedMovieIds);

        };
    }

    //gets all movies that are stored in firebase and fills the main view
    function getMovieCollectionFromFirebase() {
        var successCallback;
        var filtersPassed;

        //console.log("GET MOVIE COLLECTION FROM FIREBASE");
        //console.log(currentUserId);

        movieItemsRequestedCounter = 0;
        if (firstInit) {
            movieItemsRequestedCounter = recommendedMovieItemsLength;
        }

        successCallback = function (excludedMovieIds) {
            var path = firebaseRef.child("movies");
            path.orderByChild("timestamp").startAt(lastRetrievedMovieItemTimestampOne).limitToFirst(1).on("child_added", function (dataSnapshot) {
                lastRetrievedMovieItemTimestampOne = dataSnapshot.val().timestamp + 1;

                if (movieItemsRequestedCounter < movieItemRequestLimitMain) {
                    if (!_.contains(excludedMovieIds, (dataSnapshot.val().id.toString()))) {
                        filtersPassed = applyFilters(dataSnapshot.val());

                        if (filtersPassed) {
                            excludedMovieIds.push(dataSnapshot.val().id.toString());
                            movieItemsRequestedCounter++;

                            if (movieItemsRequestedCounter <= 4 && firstInit) {
                                initTopViewCallback([dataSnapshot.val()]);
                            } else {
                                initBottomViewCallback([dataSnapshot.val()]);
                            }
                        }
                    }
                }

                successCallback(excludedMovieIds);

                if (movieItemsRequestedCounter === movieItemRequestLimitMain) {
                    firstInit = false;
                    //console.log(movieItemsRequestedCounter);
                    onAllMovieItemsLoadedCallback(true);
                    if (movieItemRequestLimitMain != 18) {
                        movieItemRequestLimitMain = 18;
                    }
                }
            }
            );
        };

        checkForExcludedMovies(successCallback);
    }

    //checks if a movie passes the activated filters ("Action","Drama","Comedy","Romance")
    function applyFilters(movieData) {
        var filterPassed = false;

        var actionFilterActive = localStorage.getItem("actionFilterActive");
        var dramaFilterActive = localStorage.getItem("dramaFilterActive");
        var comedyFilterActive = localStorage.getItem("comedyFilterActive");
        var romanceFilterActive = localStorage.getItem("romanceFilterActive");
        var genre;
        var genreName;

        if (actionFilterActive === "true" || dramaFilterActive === "true"
            || comedyFilterActive === "true" || romanceFilterActive === "true") {

            for (genre in movieData.genres) {

                genreName = movieData.genres[genre].name;

                if (actionFilterActive === "true" && genreName === "Action") {
                    filterPassed = true;
                }

                if (dramaFilterActive === "true" && genreName === "Drama") {
                    filterPassed = true;
                }

                if (comedyFilterActive === "true" && genreName === "Comedy") {
                    filterPassed = true;
                }

                if (romanceFilterActive === "true" && genreName === "Romance") {
                    filterPassed = true;
                }
            }
            if (filterPassed) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    //gets the username that has been stored during registration
    function getCurrentUserName() {
        firebaseRef.child("users").child(currentUserId).child("username").once("value", function (dataSnapshot) {
            getCurrentUserNameCallback(dataSnapshot.val());
        });
    }

    //gets a the last two movies that have been added to the Collection for use in Preference View
    function getMovieCollectionForPreferences() {
        firebaseRef.child("movies").orderByChild("timestamp").startAt(lastRetrievedMovieItemTimestampTwo).limitToFirst(movieItemRequestLimitPreference).on("child_added", function (dataSnapshot) {
            //console.log(dataSnapshot.val());
            lastRetrievedMovieItemTimestampTwo = dataSnapshot.val().timestamp + 1;
            //currentMovieItemsIds.push(dataSnapshot.val().id);
            processMoviesForPreferencesCallback(dataSnapshot.val());
            //console.log(currentMovieItemsIds);
        });
    }

    //a movie item is saved to Firebase
    function saveMovieToFirebase(movieItem) {
        firebaseRef.child("movies").child(movieItem.id).set(movieItem);
    }

    //saves user information during registration or login to Firebase
    function saveUserDataToFirebase(userData) {
        var userInfo;

        //currentOperation = "Saving user Data to Firebase";
        currentUserId = userData.uid;

        switch (userData.provider) {
        case "facebook":
            userInfo = userData.facebook.displayName;
            break;
        case "twitter":
            userInfo = userData.twitter.displayName;
            break;
        case "google":
            userInfo = userData.google.displayName;
            break;
        case "password":
            userInfo = userData.displayName;
            break;
        default:
            userInfo = userData.displayName;
            //console.log("Error: User Data could not be saved to Firebase");
        }
        firebaseRef.child("users").child(currentUserId).once("value",function(dataSnapshot){
            if(!dataSnapshot.exists()){
                firebaseRef.child("users").child(currentUserId).set({
                    username: userInfo,
                    provider: userData.provider
                }, onComplete);
            }
        });

    }

    //a movie is upvoted and stored under the currentUsersId in Firebase
    function upVoteMovie(movieId) {
        firebaseRef.child("movieRatings").child("downvote___" + currentUserId).once("value", function (dataSnapshot) {
            if (_.values(dataSnapshot.val()).indexOf(movieId) === -1 || !dataSnapshot.exists()) {
                firebaseRef.child("movieRatings").child("upvote___" + currentUserId).once("value", function (dataSnapshot) {
                    if (_.values(dataSnapshot.val()).indexOf(movieId) === -1 || !dataSnapshot.exists()) {
                        //currentOperation = "Saving a movie that hasn't been voted before to movieRatings";
                        firebaseRef.child("movieRatings").child("upvote___" + currentUserId).push(movieId, ratingsCallback);
                    } else {
                        //console.log("Movie " + movieId + " has already been upvoted");
                    }
                });
            } else {
                //console.log("Movie " + movieId + " has already been downvoted");
            }
        });
    }

    //a movie is downvoted and stored under the currentUsersId in Firebase
    function downVoteMovie(movieId) {
        firebaseRef.child("movieRatings").child("upvote___" + currentUserId).once("value", function (dataSnapshot) {
            if (_.values(dataSnapshot.val()).indexOf(movieId) === -1 || !dataSnapshot.exists()) {
                firebaseRef.child("movieRatings").child("downvote___" + currentUserId).once("value", function (dataSnapshot) {
                    if (_.values(dataSnapshot.val()).indexOf(movieId) === -1 || !dataSnapshot.exists()) {
                        //currentOperation = "Saving a movie that hasn't been downvoted before to movieRatings";
                        firebaseRef.child("movieRatings").child("downvote___" + currentUserId).push(movieId, ratingsCallback);
                    } else {
                        //console.log("Movie " + movieId + " has already been downvoted");
                    }

                });
            } else {
                //console.log("Movie " + movieId + " has already been upvoted");
            }
        });
    }

    //gets called when all ratings have been received from Firebase
    function ratingsCallback(error) {
        //console.log("RATINGS CALLBACK");
        if (typeof preferenceRatingCallback == "function") {
            preferenceRatingCallback();
        }
        onComplete(error);
    }

    //retrieves all movielists from current user with content information
    function getAllMovieListsFromCurrentUser() {
        var key;

        firebaseRef.child("movielists").child(currentUserId).once("value", function (dataSnapshot) {
            var temp = dataSnapshot.val();
            for (key in temp) {
                onMovieListsFromCurrentUserRetrievedCallback(key, temp[key]);
            }
        });
    }

    //saves movie to a listName in Firebase
    function saveMoviesToList(movieItems, listName) {
        var i;

        if (currentUserId) {
            //currentOperation = "Saving a collection of movies to the movielist " + listName;
            firebaseRef.child("movielists").child(currentUserId).child(listName).once("value", function (dataSnapshot) {
                var temp = _.values(dataSnapshot.val());
                for (i = 0; i < movieItems.length; i++) {
                    if (temp.includes(movieItems[i].toString())) {
                        //console.log("Movie still present in movie list");
                    } else {
                        firebaseRef.child("movielists").child(currentUserId).child(listName).push(movieItems[i].toString(), onComplete);
                    }
                }
            });
        } else {
            //console.log("The user is not logged in: userId = " + currentUserId);
        }
    }

    //deletes a movie from all movielists that the current user is managing
    function deleteMoviesFromAllLists(movieItems) {
        var path;

        if (currentUserId) {
            //currentOperation = "Deleting a voted movie from all movielists";
            path = firebaseRef.child("movielists").child(currentUserId);
            path.once("value", function (dataSnapshot) {
                dataSnapshot.forEach(function (childSnapshot) {
                    deleteMoviesFromList(movieItems, childSnapshot.key());
                });
            });
        } else {
            //console.log("The user is not logged in: userId = " + currentUserId);
        }
    }

    //deletes a movie from a speific movielists (listName) that the current user is managing
    function deleteMoviesFromList(movieItems, listName) {
        var path;

        if (currentUserId) {
            //currentOperation = "Deleting a collection of movies from the movielist " + listName;
            path = firebaseRef.child("movielists").child(currentUserId).child(listName);
            path.once("value", function (dataSnapshot) {
                dataSnapshot.forEach(function (childSnapshot) {
                    if (movieItems.includes(childSnapshot.val())) {
                        path.child(childSnapshot.key()).remove(onComplete);
                    }
                });
            });

        } else {
            //console.log("The user is not logged in: userId = " + currentUserId);
        }
    }

    onComplete = function (error) {
        if (error) {
            //console.log("Firebase operation was not successfull\nOperation Details: " + currentOperation);
        } else {
            //console.log("Firebase operation was successfull!!!\nOperation Details: " + currentOperation);
        }
    };

    //retrieves the stored movieData to the corresponding movieId stored in movieItems
    function getMovieItemsFromFirebase(movieItems) {
        var result = [];
        var i = 0;
        getMovieItem(movieItems, i, result);
    }

    function getMovieItem(movieItems, i, result) {
        var temp = i;
        firebaseRef.child("movies").child(movieItems[i]).once("value", function (dataSnapshot) {
            result.push(dataSnapshot.val());
            if (i === (movieItems.length - 1)) {
                onMovieItemsRetrievedCallback(result);
                return;
            } else {
                temp++;
                getMovieItem(movieItems, temp, result);
            }
        });
    }

    function getMovieItemsFromListFromFirebase(listName, movieItems) {
        var result = [];
        var i = 0;
        getMovieItemFromList(listName, movieItems, i, result);
    }

    //gets all movieItems from a specific movielist the user is currently managing
    function getMovieItemFromList(listName, movieItems, i, result) {
        var temp = i;
        firebaseRef.child("movies").child(movieItems[i]).once("value", function (dataSnapshot) {
            result.push(dataSnapshot.val());
            if (i === (movieItems.length - 1)) {
                onMovieItemsFromListRetrievedCallback(listName, result);
                return;
            } else {
                temp++;
                getMovieItemFromList(listName, movieItems, temp, result);
            }
        });
    }

    function setInitBottomViewCallback(callback) {
        initBottomViewCallback = callback;
    }

    function setInitTopViewCallback(callback) {
        initTopViewCallback = callback;
    }

    function setProcessMoviesForPreferencesCallback(callback) {
        processMoviesForPreferencesCallback = callback;
    }

    function setOnMovieListsFromCurrentUserRetrievedCallback(callback) {
        onMovieListsFromCurrentUserRetrievedCallback = callback;
    }

    function setOnUserRatingsRetrievedCallback(callback) {
        onMovieRatingsRetrievedCallback = callback;
    }

    function setMovieItemRequestLimitMain(value) {
        movieItemRequestLimitMain = value;
    }

    function setRecommendedMovieItemsLength(value) {
        recommendedMovieItemsLength = value;
    }

    function addMovieItemsToExcludedMovieIds(movieItemsIds) {
        excludedMovieIds.push(movieItemsIds);
    }

    function setOnAllMovieItemsLoadedCallback(callback) {
        onAllMovieItemsLoadedCallback = callback;
    }

    function setOnMovieItemsRetrievedCallback(callback) {
        onMovieItemsRetrievedCallback = callback;
    }

    function setOnMovieItemsFromListRetrievedCallback(callback) {
        onMovieItemsFromListRetrievedCallback = callback;
    }

    function setPreferenceRatingCallback(callback) {
        preferenceRatingCallback = callback;
    }

    function setGetCurrentUsernameCallback(callback) {
        getCurrentUserNameCallback = callback;
    }


    that.deleteMoviesFromAllLists = deleteMoviesFromAllLists;
    that.setGetCurrentUsernameCallback = setGetCurrentUsernameCallback;
    that.getCurrentUserName = getCurrentUserName;
    that.applyFilters = applyFilters;
    that.setPreferenceRatingCallback = setPreferenceRatingCallback;
    that.setOnMovieItemsFromListRetrievedCallback = setOnMovieItemsFromListRetrievedCallback;
    that.getMovieItemsFromListFromFirebase = getMovieItemsFromListFromFirebase;
    that.getMovieItemsFromFirebase = getMovieItemsFromFirebase;
    that.setOnMovieItemsRetrievedCallback = setOnMovieItemsRetrievedCallback;
    that.setOnAllMovieItemsLoadedCallback = setOnAllMovieItemsLoadedCallback;
    that.addMovieItemsToExcludedMovieIds = addMovieItemsToExcludedMovieIds;
    that.setRecommendedMovieItemsLength = setRecommendedMovieItemsLength;
    that.setMovieItemRequestLimitMain = setMovieItemRequestLimitMain;
    that.setProcessMoviesForPreferencesCallback = setProcessMoviesForPreferencesCallback;
    that.getVotedMoviesCallback = getVotedMoviesCallback;
    that.getMovieCollectionForPreferences = getMovieCollectionForPreferences;
    that.setOnUserRatingsRetrievedCallback = setOnUserRatingsRetrievedCallback;
    that.setOnMovieListsFromCurrentUserRetrievedCallback = setOnMovieListsFromCurrentUserRetrievedCallback;
    that.getAllMovieListsFromCurrentUser = getAllMovieListsFromCurrentUser;
    that.getCurrentUserId = getCurrentUserId;
    that.getUserRatings = getUserRatings;
    that.upVoteMovie = upVoteMovie;
    that.downVoteMovie = downVoteMovie;
    that.setInitTopViewCallback = setInitTopViewCallback;
    that.setInitBottomViewCallback = setInitBottomViewCallback;
    that.deleteMoviesFromList = deleteMoviesFromList;
    that.saveMoviesToList = saveMoviesToList;
    that.saveUserDataToFirebase = saveUserDataToFirebase;
    that.init = init;
    that.getFirebaseRef = getFirebaseRef;
    that.saveMovieToFirebase = saveMovieToFirebase;
    that.getMovieCollectionFromFirebase = getMovieCollectionFromFirebase;
    return that;
};