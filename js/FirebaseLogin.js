/**
 * This module contains the functionality associated with the login procedure in firebase
 * (social media logins are implemented in SocialMediaLogins.js)
 */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.FirebaseLogin = function(arguments){
    var that = {},
        userDataCallback,
        firebaseRef = arguments.firebase,
        loginButton = arguments.loginButton,
        emailTextField = arguments.emailTextField,
        passwordTextField = arguments.passwordTextField,
        onLoginCallback;


    //set Callback when user is authenticated
    function initCallbacks(){
        firebaseRef.onAuth(authDataCallback);
    }

    //add listener to login button
    function addLoginButtonCallback(){
        loginButton.addEventListener("click", function () {
            //console.log("Login-Button Clicked");
            if(emailTextField.value === ""){
                //window.alert("Please enter a valid email address");
            }else if(passwordTextField.value === ""){
                //window.alert("Please enter a password");
            }else{
                //console.log(emailTextField.value, passwordTextField.value);
                authUser(emailTextField.value,passwordTextField.value);
            }
        });
    }

    //logout user
    function unAuthUser(isLogout){
        //console.log(firebaseRef);
        firebaseRef.unauth(authDataCallback);
        if(isLogout === true){
            parent.location = "index.html";
        }
    }

    //login user
    function authUser(email, password){
        firebaseRef.authWithPassword({
            email:      email,
            password:   password
        }, authHandler);
    }

    //called when login user tries to login
    function authHandler(error) {
        if (error) {
            //console.log("Login Failed!", error);
            //window.alert(error);
        } else {
            //console.log("Authenticated successfully with payload:", authData);
        }
    }

    function authDataCallback(authData) {
        if (authData) {
            userDataCallback(authData);
            //console.log("User " + authData.uid + " is logged in with " + authData.provider);
            localStorage.setItem("userId", authData.uid);

            onLoginCallback();
        } else {
            //console.log("User is logged out");
        }
    }

    //sends user to main.html or preference.html depending on how many movies he has already voted
    function proceedAfterLogin(data){
        if(data.length >= 10){
            parent.location = "main.html";
        }else{
            parent.location = "preference.html";
        }
    }

    function setSaveUserDataCallback(callback){
        userDataCallback = callback;
    }

    function setOnLoginCallback(callback){
        onLoginCallback = callback;
    }


    that.proceedAfterLogin = proceedAfterLogin;
    that.setOnLoginCallback = setOnLoginCallback;
    that.unAuthUser = unAuthUser;
    that.setSaveUserDataCallback = setSaveUserDataCallback;
    that.initCallbacks = initCallbacks;
    that.addLoginButtonCallback = addLoginButtonCallback;
    that.authUser = authUser;
    return that;
};