/**
 * This module is the base from which all other modules are accessed and over which callback are passed
 */

/* global  */

var MovieDatabaseApp = (function () {

    var that = {},
        algorithmSimilarityFactor = 0.1,
        firebase,
        algorithm,
        viewControllerMain,
        viewControllerPreference,
        viewControllerProfile,
        firstInit = true;

    /* initialize all elements */
    function init() {
        firebase = new MovieDatabaseApp.Firebase();
        firebase.init();
    }

    /* initialize login */
    function initLogin() {
        //console.log("Login View");
        //console.log(firebase.getCurrentUserId());
        localStorage.clear();

        var viewControllerLogin;
        var firebaseLogin;
        var firebaseRegistration;
        var socialMediaLogins;
        var movieDB;

        arguments.loginButton = document.getElementById("login-button");
        arguments.emailTextField = document.getElementById("login_email");
        arguments.passwordTextField = document.getElementById("login_password");
        arguments.facebookLoginButton = document.getElementById("facebook-login-button");
        arguments.twitterLoginButton = document.getElementById("twitter-login-button");
        arguments.googleLoginButton = document.getElementById("google-login-button");
        arguments.registerButtonInPopup = document.getElementById("sign_up_button");
        arguments.registerUsernameTextField = document.getElementById("register_name");
        arguments.registerEmailTextField = document.getElementById("register_email");
        arguments.registerPasswordTextField = document.getElementById("register_password");
        arguments.registerPopup = document.getElementById("modal");
        arguments.registerButton = document.getElementById("register-button");
        arguments.firebase = firebase.getFirebaseRef();

        viewControllerLogin = new MovieDatabaseApp.ViewControllerLogin(arguments);
        firebaseLogin = new MovieDatabaseApp.FirebaseLogin(arguments);
        firebaseRegistration = new MovieDatabaseApp.FirebaseRegistration(arguments);
        socialMediaLogins = new MovieDatabaseApp.SocialMediaLogins(arguments);
        movieDB = new MovieDatabaseApp.MovieDB();

        viewControllerLogin.init();
        movieDB.setFirebaseMovieCallback(firebase.saveMovieToFirebase);
        firebaseLogin.setSaveUserDataCallback(firebase.saveUserDataToFirebase);
        firebaseLogin.setOnLoginCallback(firebase.getUserRatings);
        firebase.setOnUserRatingsRetrievedCallback(firebaseLogin.proceedAfterLogin);
        firebaseRegistration.setAuthCallback(firebase.saveUserDataToFirebase);

        firebaseLogin.unAuthUser();
        firebaseLogin.initCallbacks();
        firebaseLogin.addLoginButtonCallback();
        firebaseRegistration.addRegistrationButtonCallback();
        socialMediaLogins.initFacebookLogin();
        socialMediaLogins.initTwitterLogin();
        socialMediaLogins.initGoogleLogin();

        //movieDB.getPopularMovies(30);
    }


    /* initialize main */
    function initMain() {
        //console.log("Main View");
        //console.log(firebase.getCurrentUserId());

        arguments.movieItemTemplate = document.getElementById("movie-entry").innerHTML;
        arguments.movieItemLocation = document.getElementById("row-container");
        arguments.movieItemTopTemplate = document.getElementById("movie-top-entry").innerHTML;
        arguments.movieItemTopLocation = document.getElementById("movie-item-top-location");
        arguments.movieItemDataTemplate = document.getElementById("movie-data-entry").innerHTML;
        arguments.movieItemDataLocation = document.getElementById("row-container");
        arguments.movieNavBarAddButton = document.getElementById("movie_list_nav_bar_add_btn");
        arguments.movieNavBarAddButtonImg = document.getElementById("movie_list_nav_bar_add_btn_img");
        arguments.addPopup = document.getElementById("shortModal");
        arguments.addPopupList = document.getElementById("list");
        arguments.addPopupTitle = document.getElementById("movie_titles");
        arguments.addPopupTextField = document.getElementById("movie_list_name");
        arguments.addPopupNewListButton = document.getElementById("new_list_btn");
        arguments.addPopupCloseButton = document.getElementById("close_btn");
        arguments.actionButton = document.getElementById("action-nav-button");
        arguments.dramaButton = document.getElementById("drama-nav-button");
        arguments.comedyButton = document.getElementById("comedy-nav-button");
        arguments.romanceButton = document.getElementById("romance-nav-button");

        algorithm = new MovieDatabaseApp.Algorithm(firebase.getFirebaseRef());
        viewControllerMain = new MovieDatabaseApp.ViewControllerMain(arguments);
        algorithm.setOnGetRecommendationsSuccessCallback(processRecomendationsCallback);
        algorithm.setOnErrorCallback(firebase.getMovieCollectionFromFirebase);

        viewControllerMain.setOnScrollEndReachedCallback(firebase.getMovieCollectionFromFirebase);
        viewControllerMain.setOnUpvoteButtonClickedCallback(firebase.upVoteMovie);
        viewControllerMain.setOnDownvoteButtonClickedCallback(firebase.downVoteMovie);
        viewControllerMain.setOnAddButtonClickedCallback(firebase.getAllMovieListsFromCurrentUser);
        viewControllerMain.setAddMovieToListCallback(firebase.saveMoviesToList);
        firebase.setOnAllMovieItemsLoadedCallback(viewControllerMain.setOnScrollEndReachedFlag);
        firebase.setOnMovieListsFromCurrentUserRetrievedCallback(viewControllerMain.addMovieListEntryToPopupCallback);
        firebase.setInitBottomViewCallback(viewControllerMain.initBottomView);
        firebase.setInitTopViewCallback(viewControllerMain.initTopView);

        viewControllerMain.addMovieNavBarAddButtonListener();
        viewControllerMain.addAddPopupListeners();
        viewControllerMain.addOnScrollEndReachedListener();
        viewControllerMain.addFilterListeners();

        algorithm.getRecommendations(algorithmSimilarityFactor);
    }

    function processRecomendationsCallback(recommendedMovieItems) {
        //console.log("PROCESS RECOMMENDATIONS");
        var item;

        var filteredRecommendedMovieItems = [];

        for (item in recommendedMovieItems) {
            if (firebase.applyFilters(recommendedMovieItems[item])) {
                filteredRecommendedMovieItems.push(recommendedMovieItems[item]);
                firebase.addMovieItemsToExcludedMovieIds(recommendedMovieItems[item].id.toString());
            }
        }

        firebase.setRecommendedMovieItemsLength(filteredRecommendedMovieItems.length);
        viewControllerMain.initRecommendations(filteredRecommendedMovieItems);
        firebase.getMovieCollectionFromFirebase();
    }

    /* initialize profile */
    function initProfile() {
        //console.log("Profile View");
        //console.log(firebase.getCurrentUserId());

        arguments.rowItemTemplate = document.getElementById("movie_list_row_entry").innerHTML;
        arguments.rowItemLocation = document.getElementById("row_location");
        arguments.movieItemDataTemplate = document.getElementById("movie-data-entry").innerHTML;
        arguments.movieItemTemplate = document.getElementById("movie-entry").innerHTML;
        arguments.wasteBinNavBarButton = document.getElementById("waste-bin-button");
        arguments.wasteBinNavBarButtonImg = document.getElementById("waste-bin-button-img");
        arguments.greetingNameField = document.getElementById("welcome_l1");


        viewControllerProfile = MovieDatabaseApp.ViewControllerProfile(arguments);
        viewControllerProfile.addNavBarDeleteButtonListener();
        viewControllerProfile.setOnDownvoteButtonClickedCallback(firebase.downVoteMovie);
        viewControllerProfile.setOnUpvoteButtonClickedCallbacK(firebase.upVoteMovie);
        viewControllerProfile.setOnMovieItemsRemovedCallback(firebase.deleteMoviesFromList);
        viewControllerProfile.setOnMovieItemRemovedFromAllListsCallback(firebase.deleteMoviesFromAllLists);

        firebase.setGetCurrentUsernameCallback(viewControllerProfile.initGreetingsNameField);
        firebase.setOnMovieListsFromCurrentUserRetrievedCallback(viewControllerProfile.init);
        viewControllerProfile.setGetMovieListItemsCallback(firebase.getMovieItemsFromListFromFirebase);
        firebase.setOnMovieItemsFromListRetrievedCallback(viewControllerProfile.initRow);

        firebase.getCurrentUserName();
        firebase.getAllMovieListsFromCurrentUser();
    }


    /* initialize preference */
    function initPreference() {
        //console.log("Preference View");
        //console.log(firebase.getCurrentUserId());

        arguments.okButton = document.getElementById("ok_btn");
        arguments.infoContainer = document.getElementById("info_container");
        arguments.backgroundImg = document.getElementById("background_img_content");
        arguments.poster = document.getElementById("poster");
        arguments.likeButton = document.getElementById("like-button");
        arguments.nextButton = document.getElementById("next-button");
        arguments.dislikeButton = document.getElementById("dislike-button");

        viewControllerPreference = new MovieDatabaseApp.ViewControllerPreference(arguments);
        viewControllerPreference.setOnNextButtonClickedCallback(firebase.getMovieCollectionForPreferences);
        viewControllerPreference.setOnLikeButtonClickedCallback(firebase.upVoteMovie);
        viewControllerPreference.setOnDislikeButtonClickedCallback(firebase.downVoteMovie);
        firebase.setProcessMoviesForPreferencesCallback(processMoviesForPreferenceCallback);
        firebase.setPreferenceRatingCallback(viewControllerPreference.processRatingsCallback);

        viewControllerPreference.addListeners();
        firebase.getMovieCollectionForPreferences();
    }

    function processMoviesForPreferenceCallback(data) {
        viewControllerPreference.addItemsToList(data);
        if (firstInit) {
            viewControllerPreference.initView();
            firstInit = false;
        }
    }

    that.initProfile = initProfile;
    that.initPreference = initPreference;
    that.initLogin = initLogin;
    that.initMain = initMain;
    that.init = init;
    return that;
}());