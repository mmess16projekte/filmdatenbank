/**
 * This module contains the logic of the register popup and the functionality to register a user with Firebase
 * and save the user data to Firebase
 */

/* global $ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.FirebaseRegistration = function(arguments){
    var that = {},
        authCallback,
        firebaseRef = arguments.firebase,
        popup = arguments.registerPopup,
        registerButton = arguments.registerButtonInPopup,
        usernameTextField = arguments.registerUsernameTextField,
        emailTextField = arguments.registerEmailTextField,
        passwordTextField = arguments.registerPasswordTextField;


    //add all listeners to the register popup
    function addRegistrationButtonCallback(){
        registerButton.addEventListener("click", function () {
            //console.log("Register-Button Clicked");
            if(usernameTextField.value === "") {
                window.alert("Please enter a valid username");
            }else if(emailTextField.value === "") {
                window.alert("Please enter a valid email");
            }else if(passwordTextField.value === "") {
                window.alert("Please enter a valid password");
            }else{
                createFirebaseUser(usernameTextField.value, emailTextField.value, passwordTextField.value);
            }
        });
    }

    //registers the user with firebase and stores the data
    function createFirebaseUser(username, email, password){
        firebaseRef.createUser({
            email    : email,
            password : password,
            username : username
        }, function(error, userData) {
            if (error) {
                //console.log("Error creating user:", error);
                window.alert(error);

            } else {
                //console.log("Successfully created user account with uid:", userData.uid);
                userData.displayName = username;
                userData.email = email;
                userData.provider = "password";
                authCallback(userData);

                //TODO: auslagern in index.js und dann über Callback
                $(document).ready(function (){
                    $("#" + popup.id).slideUp(1000);
                });

            }
        });
    }

    function setAuthCallback(callback){
        authCallback = callback;
    }


    that.setAuthCallback = setAuthCallback;
    that.createFirebaseUser = createFirebaseUser;
    that.addRegistrationButtonCallback = addRegistrationButtonCallback;
    return that;
};