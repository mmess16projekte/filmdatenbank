/**
 * This module is used to retrieve data from TMDB and merge them with OMDB results.
 * The merged results will later be stored in Firebase
 */

/* global $ */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.MovieDB = function () {
    var that = {},
        requestCounter = 0,
        tmdbMovieItems = [],
        movieItems = [],
        pagesCounter = 1,
        pagesLimit,
        movieItemsCounter = 0,
        movieItemsLimit,
        addMovieToFirebaseCallback;


    //gets a number of pages from popular movies (in TMDB)
    function getPopularMovies(numPages) {
        //console.log("Pages: "+numPages);
        pagesLimit = numPages;
        requestCounter++;
        getPopularMoviesPage(pagesCounter);
    }

    //get one page from popular movies (in TMDB)
    function getPopularMoviesPage(page){
        theMovieDb.movies.getPopular({page: page}, getPopularMoviesSuccessCallback, errorCB);
    }

    //processes the retrieved page information
    function getPopularMoviesSuccessCallback(data) {
        pagesCounter++;
        tmdbMovieItems = tmdbMovieItems.concat(JSON.parse(data).results);
        if (pagesCounter === pagesLimit) {
            //console.log(tmdbMovieItems);
            movieItemsLimit = tmdbMovieItems.length;
            getMovieWithId(tmdbMovieItems[movieItemsCounter].id);
        }else{
            requestCounter++;
            handleRequestLimit();
            getPopularMoviesPage(pagesCounter);
        }
    }

    //get a movie with a specific id
    function getMovieWithId(movieId) {
        //console.log(requestCounter+"    "+movieId);
        theMovieDb.movies.getById({"id": movieId}, getMovieWithIdSuccessCallback, errorCB);
    }

    //process movie information from TMDB and merge them with ODMB to later save the result to firebase
    function getMovieWithIdSuccessCallback(data) {
        var tmdbResult;
        var omdbResult;
        var result;

        movieItemsCounter++;
        tmdbResult = JSON.parse(data);
        omdbResult = JSON.parse(httpGet("http://www.omdbapi.com/?i=" + tmdbResult.imdb_id + "&plot=short&r=json"));
        result = tmdbResult;
        if (omdbResult.Response === "True") {
            result = mergeDBResults(tmdbResult, omdbResult);
        }
        result.original_poster_path = "http://image.tmdb.org/t/p/original"+result.poster_path;
        result.poster_path = "http://image.tmdb.org/t/p/w300"+result.poster_path;
        result.backdrop_path = "http://image.tmdb.org/t/p/w300"+result.backdrop_path;
        result.timestamp = Date.now();
        addMovieToFirebaseCallback(result);
        movieItems = movieItems.concat(result);
        //console.log(movieItemsCounter);
        if(movieItemsCounter === movieItemsLimit){
            //console.log("FINAL");
            //console.log(movieItems);
        }else{
            requestCounter++;
            handleRequestLimit();
            getMovieWithId(tmdbMovieItems[movieItemsCounter].id);
        }
    }

    //merges the results from TMDB and OMDB
    function mergeDBResults(tmdbResult, omdbResult) {
        var result = {};
        $.extend(result, tmdbResult, omdbResult);
        return result;
    }

    //waits for 10 seconds until requests are possible again
    function handleRequestLimit(){
        if (requestCounter%40 === 0) {
            //console.log("Wait for 10 sec");
            sleepFor(10000);
        }
    }

    function errorCB() {
        //console.log("Error callback: " + data);
    }

    //used to retrieve data from OMDB
    function httpGet(theUrl) {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("GET", theUrl, false);
        xmlHttp.send(null);
        return xmlHttp.responseText;
    }

    //stops all operations for a given duration
    function sleepFor(sleepDuration) {
        var now = new Date().getTime();

        while (new Date().getTime() < now + sleepDuration) {
        }
    }

    function setFirebaseMovieCallback(callback) {
        addMovieToFirebaseCallback = callback;
    }


    that.getPopularMovies = getPopularMovies;
    that.getMovieWithId = getMovieWithId;
    that.setFirebaseMovieCallback = setFirebaseMovieCallback;
    return that;
};
