/**
 * This module contains the functionality of all social media based logins such as Facebook, Twitter and Google
 */

var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.SocialMediaLogins = function (arguments) {
    var that = {},
        firebase = arguments.firebase,
        facebookLoginButton = arguments.facebookLoginButton,
        twitterLoginButton = arguments.twitterLoginButton,
        googleLoginButton = arguments.googleLoginButton,
        onLoginSuccessCallback;


    //adds a listener to the facebook button and logs the user in to facebook when fired
    function initFacebookLogin() {
        facebookLoginButton.addEventListener("click", function () {
            //console.log("Facebook Login Button Clicked");
            firebase.authWithOAuthPopup("facebook", function (error) {
                if (error) {
                    //console.log("Login Failed!", error);
                } else {
                    //console.log("Authenticated successfully with payload:", authData);
                }
            });
        });
    }

    //adds a listener to the twitter button and logs the user in to twitter when fired
    function initTwitterLogin() {
        twitterLoginButton.addEventListener("click", function () {
            //console.log("Twitter Login Button Clicked");
            firebase.authWithOAuthPopup("twitter", function (error) {
                if (error) {
                    //console.log("Login Failed!", error);
                } else {
                    //console.log("Authenticated successfully with payload:", authData);
                }
            });

        });
    }

    //adds a listener to the google button and logs the user in to google when fired
    function initGoogleLogin() {
        googleLoginButton.addEventListener("click", function () {
            //console.log("Google Login Button Clicked");
            firebase.authWithOAuthPopup("google", function (error) {
                if (error) {
                    //console.log("Login Failed!", error);
                } else {
                    //console.log("Authenticated successfully with payload:", authData);
                }
            });

        });
    }

    function setOnLoginSuccessCallback(callback){
        onLoginSuccessCallback = callback;
    }

    that.setOnLoginSuccessCallback = setOnLoginSuccessCallback;
    that.initFacebookLogin = initFacebookLogin;
    that.initTwitterLogin = initTwitterLogin;
    that.initGoogleLogin = initGoogleLogin;
    return that;
};