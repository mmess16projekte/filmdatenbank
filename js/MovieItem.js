/**
 * This module is used to group all properties of a movie item that are needed for processing
 */
var MovieDatabaseApp = MovieDatabaseApp || {};

MovieDatabaseApp.MovieItem = function () {
    var that = {},
        movieId,
        container,
        cancelButton,
        checkButton,
        addButton,
        upVoteButton,
        downVoteButton,
        coverImage,
        alreadyVoted,
        cancelButtonVisible,
        checkButtonVisible,
        parentNode,
        topNode,
        listName,
        buttonsVisibleInMain,
        markAllButtonImg;


    function getMovieId(){
        return movieId;
    }

    function getListName(){
        return listName;
    }

    function getContainer(){
        return container;
    }

    function getCancelButton(){
        return cancelButton;
    }

    function getCheckButton(){
        return checkButton;
    }

    function getAddButton(){
        return addButton;
    }

    function getUpVoteButton(){
        return upVoteButton;
    }

    function getDownVoteButton(){
        return downVoteButton;
    }

    function getCoverImage(){
        return coverImage;
    }

    function getAlreadyVoted(){
        return alreadyVoted;
    }

    function getCancelButtonVisible(){
        return cancelButtonVisible;
    }

    function getCheckButtonVisible(){
        return checkButtonVisible;
    }

    function getParentNode(){
        return parentNode;
    }

    function getTopNode(){
        return topNode;
    }

    function getButtonsVisibleInMain() {
        return buttonsVisibleInMain;
    }

    function getMarkAllButtonImg(){
        return markAllButtonImg;
    }

    function setMovieId(value){
        movieId = value;
    }

    function setListName(value){
        listName = value;
    }

    function setContainer(value){
        container = value;
    }

    function setCancelButton(value){
        cancelButton = value;
    }

    function setCheckButton(value){
        checkButton = value;
    }

    function setAddButton(value){
        addButton = value;
    }

    function setUpVoteButton(value){
        upVoteButton = value;
    }

    function setDownVoteButton(value){
        downVoteButton = value;
    }

    function setCoverImage(value){
        coverImage = value;
    }

    function setAlreadyVoted(value){
        alreadyVoted = value;
    }

    function setCancelButtonVisible(value){
        cancelButtonVisible = value;
    }

    function setCheckButtonVisible(value){
        checkButtonVisible = value;
    }

    function setParentNode(value){
        parentNode = value;
    }

    function setTopNode(value){
        topNode = value;
    }

    function setButtonsVisibleInMain(value) {
        buttonsVisibleInMain = value;
    }

    function setMarkAllButtonImg(value){
        markAllButtonImg = value;
    }


    that.getMovieId = getMovieId;
    that.getListName = getListName;
    that.getContainer = getContainer;
    that.getCancelButton = getCancelButton;
    that.getCheckButton = getCheckButton;
    that.getAddButton = getAddButton;
    that.getUpVoteButton = getUpVoteButton;
    that.getDownVoteButton = getDownVoteButton;
    that.getCoverImage = getCoverImage;
    that.getAlreadyVoted = getAlreadyVoted;
    that.getCancelButtonVisible = getCancelButtonVisible;
    that.getCheckButtonVisible = getCheckButtonVisible;
    that.getParentNode = getParentNode;
    that.getTopNode = getTopNode;
    that.getButtonsVisibleInMain = getButtonsVisibleInMain;
    that.getMarkAllButtonImg = getMarkAllButtonImg;
    that.setMovieId = setMovieId;
    that.setListName = setListName;
    that.setContainer = setContainer;
    that.setCancelButton = setCancelButton;
    that.setCheckButton = setCheckButton;
    that.setAddButton = setAddButton;
    that.setUpVoteButton = setUpVoteButton;
    that.setDownVoteButton = setDownVoteButton;
    that.setCoverImage = setCoverImage;
    that.setAlreadyVoted = setAlreadyVoted;
    that.setCancelButton = setCancelButton;
    that.setCancelButtonVisible = setCancelButtonVisible;
    that.setCheckButtonVisible = setCheckButtonVisible;
    that.setParentNode = setParentNode;
    that.setTopNode = setTopNode;
    that.setButtonsVisibleInMain = setButtonsVisibleInMain;
    that.setMarkAllButtonImg = setMarkAllButtonImg;
    return that;
};