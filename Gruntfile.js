module.exports = function(grunt){

    grunt.initConfig({
        concat: {
            js: {
                src: [  "js/index.js",
                        "js/Algorithm.js",
                        "js/Firebase.js",
                        "js/FirebaseLogin",
                        "js/FirebaseRegistration.js",
                        "js/MovieDB.js",
                        "js/MovieItem.js",
                        "js/SocialMediaLogins.js",
                        "js/TMDB.js",
                        "js/ViewContollerLogin.js",
                        "js/ViewControllerMain.js",
                        "js/ViewControllerPreference.js",
                        "js/ViewControllerProfile.js",
                        "libs/alertify.min.js",
                        "libs/bootstrap.js",
                        "libs/jquery.leanModal.min.js",
                        "libs/jquery-2.1.0.min.js",
                        "libs/underscore-min.js"            ],

                dest: "build/script.js",
            },

            css: {
                src: [  "css/animate.css",
                        "css/bootstrap.css",
                        "css/index_styles.css",
                        "css/main_styles.css",
                        "css/movieData_template.css",
                        "css/movieItem_profile_template.css",
                        "css/preference.css",
                        "css/profile_styles.css",
                        "libs/alertify.default.min.css",
                        "libs/alertify.min.css",            ],

                dest: "build/stylesheet.css",
            },

        },
        watch: {
            js: {
                files: ["js/**/*.js"],
                tasks: ["concat:js"], //concat:js spricht nur alle änderungen in js files an, nur concat überschreibt alle Dateien auch css
            },
            css: {
                files: ["css/**/*.js"],
                tasks: ["concat"],
            },
        },
    });



    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.registerTask("default", ["concat", "watch"]);
};